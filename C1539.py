import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


# Link for test case: https://monitorsoft.testrail.io/index.php?/cases/view/1539

options = webdriver.ChromeOptions()
capabilities = options.to_capabilities()
driver = webdriver.Remote(command_executor='http://selenium:4444/wd/hub', desired_capabilities=capabilities)


driver.get("http://google.com")
time.sleep(5)
driver.save_screenshot('Screenshots/docker_image_1.png')
elem = driver.find_element_by_xpath('/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input')
elem.send_keys("Cat memes")
elem.send_keys(Keys.RETURN)
time.sleep(5)
driver.save_screenshot('Screenshots/docker_image_2.png')
time.sleep(5)
driver.find_element_by_xpath('/html/body/div[7]/div/div[3]/div/div[1]/div/div[1]/div/div[2]/a').click()
time.sleep(5)
driver.save_screenshot('Screenshots/docker_image_3.png')
driver.quit()